import paho.mqtt.client as mqtt
import threading
import numpy as np
import cv2

import matplotlib.pyplot as plt
import cvlib as cv
from cvlib.object_detection import draw_bbox

import base64
import json
import codecs
import hashlib

import pickle

import time

import zlib

MQTTClient = mqtt.Client()


def on_connect(client, userdata, flags, rc):
    print("Connected to Hetzner MQTT")

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("/hetzner/in/image")
    client.publish("/hetzner/status/", "online")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    client.publish("/hetzner/status/", "msg reveived")
    if msg.topic == "/hetzner/in/image":
        analyze_picture(msg.payload)



def mqtt_client():
    MQTTClient.on_connect = on_connect
    MQTTClient.on_message = on_message
    MQTTClient.username_pw_set("hetzner", password="ECY8EQWmFpf6xXLe")
    MQTTClient.connect("localhost", 1883, 60)

    # Blocking call that processes network traffic, dispatches callbacks and
    # handles reconnecting.
    # Other loop*() functions are available that give a threaded interface and a
    # manual interface.
    MQTTClient.loop_forever()
    return



t_loc = threading.Thread(target=mqtt_client, name="MQTT_Local")
t_loc.start()


def analyze_picture(msg):

    start_time = time.time()

    base64String = json.loads(msg)["image"]

    im = pickle.loads(zlib.decompress(base64.b64decode(codecs.encode(base64String))))
    
    crop_img = im[115:344, 119:900]

    bbox, label, conf = cv.detect_common_objects(crop_img)
    tempArr = []

    for index, item in enumerate(label):
        if item != "car" and item != "truck":
            tempArr.append(index)

    tempArr.sort(reverse=True)
    for index in tempArr:
        del label[index]
        del bbox[index]
        del conf[index]

    crop_img_BASE64 = codecs.decode(base64.b64encode(pickle.dumps(crop_img)))

    mqtt_msg = json.dumps({'crop_img': crop_img_BASE64, 'bbox': bbox, 'label': label, 'conf': conf})
    MQTTClient.publish("/hetzner/out/analysis", mqtt_msg)

    print("Analysis took ", round(time.time() - start_time, 3), "s to run")
    



    
